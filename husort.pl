#! /bin/sh
eval '(exit $?0)' && eval 'PERL_BADLANG=x;PATH="$PATH:.";export PERL_BADLANG\
;exec perl -x -S -- "$0" ${1+"$@"};#'if 0;eval 'setenv PERL_BADLANG x\
;setenv PATH "$PATH":.;exec perl -x -S -- "$0" $argv:q;#'.q
#!perl -w
+push@INC,'.';$0=~/(.*)/s;do(index($1,"/")<0?"./$1":$1);die$@if$@__END__+if 0
;#Don't touch/remove lines 1--7: http://www.inf.bme.hu/~pts/Magic.Perl.Header
#
# husort.pl -- Hungarian makeindex replacement for TeX
# by pts@fazekas.hu at Sat Nov 23 12:21:07 CET 2002
# at Sat Nov 23 20:42:25 CET 2002
# full T1 at Sun Nov 24 18:23:00 CET 2002
# docs at Sun Nov 24 20:03:46 CET 2002
# makeindex compatibility at Sun Dec  1 18:18:56 CET 2002
# v0.03 better \indexspace at Tue Aug  5 23:38:37 CEST 2003
# v0.04 better interval handling at Thu Aug  7 22:48:24 CEST 2003
# v0.05 better |run handling
# v0.06 output consistency check at Mon Apr 19 17:43:21 CEST 2004
# v0.07 better makeindex-compatible line splitting at Wed Apr 21 09:23:34 CEST 2004
# v0.08 added \@empty and \empty support and latin2 input encoding at Tue May 11 20:56:49 CEST 2004
#
# Imp: support {\rm123} in hu_t1_init() (?? maybe done |rm| in t1_pres2alpha??)
# Imp: make parts of t1_pres2alpha() optional
# Imp: add style file support, multilanguage (Hungarian, English, German) support etc.
# Imp: option to support Hungarian S5b. (consonant_equiv)
# Imp: develop a Xindy module to reproduce some effects
# Imp: separate index of poems (irodalmi versek mutat�ja)
# Imp: separate index of names (glossary?)
# Imp: add option aggregate_tags
# Imp: BUGFIX: s�r�s�gf�ggv�ny (where??)
# OK : �kezetes m�ssalhangz�k (T1-beliek)
# OK : `!' levels with tilde: id�!~fogalma, id�!~jelz�s, id�!t�r �s ~, id�!g�p~
# OK : `\lowtilde{}' and `\~{}' display a beutiful, lowered tilde, which is
#      whose alphabetic value is the previous subword
# OK : `!' levels
# OK : makeindex-like `@' presentation and sort order override : \index{alpha@$\alpha$}
# OK : warning on intervals \( .. \) roman <-> arabic
# OK : \index{at!bat|see{bat, at}}
# OK : \index{gnat|(}
# OK : roman page numbers
# OK : letter group title 'E, �'
# OK : idegen tulajdonnevek �kezetes bet�i
# OK : add important-space ({ }) for `Horv�t{ }Zolt�n < Horv�th{ }Andor', and
#      still `Nagy{ }Albert < Nagy{ }A. M�rton'
# OK : \index{foo|(hyperpage} etc. compatibility with makeindex
# OK : no more makeindex incompatibility: `\indexx{at!bat|see{bat, at}}' always writes 0 as page number, i.e `\see{...}{0}'
# OK : respects \index{|run husort.pl -r} etc.
# OK : -r option in the .idx file (\index{|run husort.pl -r})
# Dat: the makeindex way of quoting \! is "\"! -- so is our way
# Dat: unknown page numbers are ordered as sort() binary; roman < arabic < unknown
# Dat: please type either `\~{}' or `\lowtilde{}'
# Dat: germandlbs, Auml and auml added
# Dat: Hungarian letters are in ISO-8859-2 (o" is �, u" is �)
# Dat: documented wrong answer, should be: m�zszer� < meztelen
# Dat: `� < �' is a property of ISO-8859-2 and T1
# Dat: `A < a' is a property of US-ASCII
# Dat: we must accept \indexentry{part1|(hyperpage}
#
# Incompatibilities between makeindex(1) and husort.pl:
#
# Dat: makeindex(1) rejects \indexentry{a|b@c}, husort.pl gives a warning
# Dat: makeindex splits long \item etc. lines, husort.pl doesn't
# Dat: makeindex ignores space in the beginning of the page number
# Dat: makeindex ignores `-q' with `-i'
# Dat: makeindex starts reading STDIN without args, husort.pl displays help
# Dat: makeindex has equivalent \index{a|)c} and \index{a|)}; husort.pl
#      emulates this using %open_last, but \index{a|)c} is recommended
# Dat: makeindex doesn't accept multiple opens: \index{a|(b} \index{a|(c}
# Dat: makeindex incompatibility: `"' in `\indexentry{..."...}{...}' quotes only [\@!|"]
# Dat: makeindex incompatibility: distinguishing STDOUT and STDERR
# Dat: `makeindex -qi' OK, but only `husort.pl -q -i' OK
# Dat: `makeindex foo' looks for foo and then foo.idx; `husort.pl' looks for foo.idx only if no extension given
# Dat: `makeindex -sgind' is wrong (`makeindex -s gind' is right), but `husort.pl -sgind' is good
# Dat: makeindex writes infos to STDERR, husort.pl writes them to STDOUT
# Dat: makeindex supports multiple input .idx files, husort.pl doesn't
# Dat: makeindex scans stdin if !@ARGV, husort.pl prints help message (needs `husort.pl -i')
# Dat: makeindex doesn't accept \index{a|b|textbf}, husort.pl emits `\item a|b'
# Dat: no log (.ilg) in husort.pl
# Dat: husort.pl doesn't emit warning `Conflicting entries: multiple encaps for
#      the same page under same key.', because it's completely legal to have
#      a bold and an italic page number to the same page
# Dat: terminilogy: \indexentry{a@b!c\~{}d|(textbf} defines an index entry with
#      two subwords: `a@b' and `c\~{}d', an open tag `(' and a tag `textbf'. The
#      alphabetic string of subword `a@b' is `a', and of `c' is `cad'. The
#      presentation subword of `a@b' is `b', and of `c\~{}d' is `c\~{}d'.
#
BEGIN { eval { require integer } and import integer }
BEGIN { eval { require strict  } and import strict  }  # go on if missing
BEGIN { $main::VERSION='0.08'; }

# --- TeX T1 encoding, used by TeX EC fonts

# Dat: missing deliberately: textvisiblespace 32
# Dat: `u 8' is \u{} in @latin2_decode
my %t1_codes=qw(
  u 8  " 4  ' 1  v 7  H 5  . 10
  { 123  } 125  % 37  & 38  ~ 126  ^ 94  $ 36  % 37  & 38  _ 95
  ! 32 thinspace 32 negthinspace 32 enspace 32 enskip 32 quad 32 qquad 32
  AE 198 DH 208 DJ 208 L 138 NG 141 OE 215 O 216 SS 223 TH 222 ae 230 dh 240
  dj 158 i 25 j 26 l 170 ng 173 oe 247 o 248 th 254
  guillemotleft 19 guillemotright 20 guilsinglleft 14 guilsinglright 15
  quotedblbase 18 quotesinglbase 13
  ss 255 textasciicircum 94 textasciitilde 126 textbackslash 92 textbar 124
  textbraceleft 123 textbraceright 125 textcompwordmark 23 textdollar 36
  textemdash 22 textendash 21 textexclamdown 189 textgreater 62 textless 60
  textquestiondown 190 textquotedblleft 16 textquotedblright 17 textquotedbl 34
  textquoteleft 96 textquoteright 39 textsection 159 textsterling 191
  textunderscore 95
  "{A 196 "{E 203 "{I 207 "{O 214 "{U 220 "{Y 152 "{a 228 "{e 235 "{\\i 239
  "{i 239 "{o 246 "{u 252 "{y 184 '{A 193 '{C 130 '{E 201 '{I 205 '{L 136
  '{N 139 '{O 211 '{R 143 '{S 145 '{U 218 '{Y 221 '{Z 153 '{a 225 '{\\i 237
  '{c 162 '{e 233 '{i 237 '\i 237
  '{l 168 '{n 171 '{o 243 '{r 175 '{s 177 '{u 250
  '{y 253 '{z 185 .{I 157 .{Z 155 .{i 25  .{z 187 ^{\\i 238 ^\\i 238 ^{i 238
  H{O 142 H{U 150 H{o 174 H{u 182 ^{E 202 ^{I 206 ^{O 212 ^{U 219 ^{A 194
  ^{a 226 ^{e 234 ^{i 238 ^{o 244 ^{u 251
  `{A 192 `{E 200 `{I 204 `{O 210 `{U 217 `{a 224 `{e 232 `{i 236 `{\\i 236
  `{o 242 `{u 249 c{C 199 c{S 147 c{T 149 c{c 231 c{s 179 c{t 181 k{A 129
  k{E 134 k{a 161 k{e 166 r{A 197 r{U 151 r{a 229 r{u 183 u{A 128 u{G 135
  u{a 160 u{g 167 v{C 131 v{D 132 v{E 133 v{L 137 v{N 140 v{R 144 v{S 146
  v{T 148 v{Z 154 v{c 163 v{d 164 v{e 165 v{l 169 v{n 172 v{r 176 v{s 178
  v{t 180 v{z 186 ~{A 195 ~{N 209 ~{O 213 ~{a 227 ~{n 241 ~{o 245
);
while (my($k,$v)=each%t1_codes) { $t1_codes{$k}=chr$v }
$t1_codes{' '}=' ';
$t1_codes{','}=' '; # avoid Perl warning: Possible attempt... (\, -> space)
$t1_codes{'#'}='#'; # avoid Perl warning: Possible attempt...
$t1_codes{textvisiblespace}=chr 160; # '{ }'; # !! why, how? !!
# vvv for latin2_decode
$t1_codes{textcurrency}='\textcurrency ';
$t1_codes{S}='\S ';
$t1_codes{textdegree}='\textdegree ';
$t1_codes{nobreakspace}='\nobreakspace ';
$t1_codes{'\times$'}='$\times $';
$t1_codes{'\div$'}='$\div $';
$t1_codes{'-'}='';
$t1_codes{'c\\'}=chr 11; # {c\ }
$t1_codes{'k\\'}=chr 12; # {c\ }
$t1_codes{IeC}='';

# Dat: \TeX T1 PostScript mapping of accents:
# \`0/grave \'1/acute \^2/circumflex \~3/tilde \"4/dieresis \H5/hungarumlaut
# \r6/ring \v7/caron \u8/breve \=9/macron \.10/dotaccent
# \c11/cedilla \k12/ogonek

#  \' {\i{}}
# IeC{\\`\\i 236 `\\i 236
# IeC{\\'\\i 237 '\\i 237
# IeC{\\^\\i 238 ^\\i 238
# IeC{\\"\\i 239 "\\i 239

#** @param $_[0] a TeX letter-command without backslash, such as '" o'
#** @param $_[1] prev word
sub t1_chr($$) {
  my $S=$_[0];
  my $T=$_[1];
  my $O=$S;
  die if !defined $S; # wrong regexp
  # die $S;
  if (1<length$S) {
    $S=~s@^IeC\s*\{\\(.)\s*@$1 @;
    $S=~s@[{}\s]+$@@;
    $S=~s@[\s{]+@{@;
    $S=~s@\A(\W)(\w)\Z(?!\n)@$1\{$2@; # '"o' -> '"{o'
    # Dat: converts ~{} to ~
  }
#  print STDERR "$S;$T;\n";
  return $T if $S eq '~' or $S eq 'lowtilde';
  ## print "($S.\n";
  if (!exists $t1_codes{$S}) { 
    print "$0: warning: T1 unknown: $O\n";
    return "";
  }
  $t1_codes{$S}
}

#** For ligature composition from ASCII to T1.
my %t1_ligatures=split':', "`:`:':':``:\020:'':\021:,,:\022:<<:\023:>>:".
  "\024:--:\025:---:\026:";

#** Generate the alphabetic string from the presentation string:
#** -- add space after control sequence names (SUXX: even after names inside
#**    \verb*)
#** -- compose T1 ligatures into T1 slots (doesn't affect newline: \012)
#** -- handle input "-quoting
#** -- transform `\" o' -> `�', `\textasciicircum ' -> `^' etc.,
#**    remove braces, remove \bf etc., \textbf etc., \emph etc.,
#**    remove \verb and \verb*, change \% to %, change ~ to space etc.
#**    the following escapes are compressed to a single character:
#**    \~{} \^{} \# \$ \% \& \{ \} \_
#** -- but don't remove \relax, so the user can affect ordering
#** -- but don't remove `{ }' and `{.}' (important space and important dot)
#** -- but don't compress multiple spaces into one
#** -- unknown control sequences won't be removed
#** @param $_[0] the presentation string
#** @param $_[1] previous word, to be substituted for \lowtilde and \~{}
#** @return the alphabetic string: $_[0] with TeX macros representing T1
#           characters substitued to single characters
sub t1_pres2alpha($;$) {
  my $S=$_[0];
  my $prev_word=defined $_[1] ? $_[1] : '~';
  die if !defined $S;
  $S=~s@\\([a-zA-Z\@]+)\s*@\\$1 @g;  # Add a space after control sequences.
  # ^^^ Dat: old: $S=~s@\\([a-zA-Z\@]+|[!-~])\s*@\\$1 @g;
  # ^^^ Dat: old: `17\% -os t�rv�ny' produced an extra space
  #$S=~s@(\\[,`'<>-]|\\""|(``?|''?|,,|<<|>>|---?|\\~\{}|"[\@!|"]))@
  $S=~s@(\\[,`'<>-]|\\""|(``?|''?|,,|<<|>>|---?))@
    defined($2) && length($2)?$t1_ligatures{$2}:$1@ge;
  # ^^^ Dat: ~\{}| -> \lowtilde; (pts extension)
  # ^^^ Dat: "@ -> @ etc.; (pts extension, simulate makeindex quoting)
  # vvv Dat: '\^ a' and '\^a' are equivalent in LaTeX
  # vvv Dat: \index{\bfseries@\verb+\bfseries+} is wrong -- use \index{\verb+\bfseries+} or \index{\ bfseries@\verb+\bfseries+}
  $S=~s@\\(  # this is a really long regsub; $1
   [\%{}&\#\$_ ,!]| # remove these characters altogether
   [`'^"]\s*(?:\\i|[{]\s*\\i\s*(?:[{][}])?[}])\s*|
    H\s*(?:\s[OUou]|[{]\s*[OUou][}])|
    c\s*(?:\s[CSTcst]|[{]\s*[CSTcst][}])|
    k\s*(?:\s[AEae]|[{]\s*[AEae][}])|
    r\s*(?:\s[AUau]|[{]\s*[AUau][}])|
    u\s*(?:\s[AGag]|[{]\s*[AGag][}])|
    v\s*(?:\s[CDELNRSTZcdelnrstz]|[{]\s*[CDELNRSTZcdelnrstz][}])|
   \^\s*(?:[AEIOUaeiou]|[{]\s*[AEIOUaeiou]?[}])| # Dat: \^{} -> ~
    "\s*(?:[AEIOUYaeiouy]|[{]\s*[AEIOUYaeiouy][}])|
    '\s*(?:[ACEILNORSUYZaceilnorsuyz]|[{]\s*[ACEILNORSUYZaceilnorsuyz][}])|
    [.]\s*(?:[IZiz]|[{]\s*[IZiz][}])|
    `\s*(?:[AEIOUaeiou]|[{]\s*[AEIOUaeiou][}])|
    ~\s*(?:[ANOano]|[{]\s*[ANOano]?[}])| # Dat: \~{} -> ~ or \lowtilde
   # IeC\s*[{]\\['"`^]\s*\\i\s*[}]| # Dat: LaTeX2e <1998/06/01> Babel <v3.6j> magyar.ldf: � -> \IeC {\'\i }
   IeC\s+| # Dat: \IeC {\'a} generated by \usepackage[utf8]{inputenc} in TeX-Live 2008
   (?:AE|D[HJ]|[ijlL]|NG|OE?|SS|TH|ae|d[hj]|guil(?:lemot|singl)(?:lef|righ)t|
    ng|oe?|quote(?:dbl|singl)base|ss|th|thinspace|negthinspace|enspace|
    enskip|quad|qquad|text(?:
      ascii(?:circum|tilde)|ba(?:ckslash|r)|brace(?:lef|righ)t|compwordmark|
      dollar|e[mn]dash|exclamdown|greater|less|questiondown|section|sterling|
      quote(?:dbl)?(?:lef|righ)t|underscore|visiblespace))\b\s*|
   lowtilde\s*\{\}|
   (text(?:bf|[ti]t|rm|md|s[cf]|up|normal)|emph|bf|ti|tt|rm|s[cf]|rmfamily|
      sffamily|ttfamily|normalfont|bfseries|mdseries|itshape|slshape|
      scshape|upshape|bseries|usefont|selectfont|
      abreak|PexaAllowBreak|allowbreak|nobreak|break|-| # Imp: don't hide space from`\- '
      normalsize|small|footnotesize|scriptsize|tiny|large|Large|LARGE|huge|Huge|
      # \@empty (see below, in line (\@empty)
      textsu(?:per|b)script|textcircled)\b\s*) # $2
  |(?:\\verb\s*[*]?(.)(.*?)\3) # $3$4
  |([{][ .][}]|[{}]) # $5 important space and important dot -- and braces
  |([~]) # $6
  @ #print"$1;$2;$3;$4;$5;$6.\n";
    defined$2 && length$2 ?"":
    defined$3 && length$3 ? $4 : # \verb*
    defined$5 && length$5 ? (length$5>1 ? $5 : "") : # braces: { }
    defined$6 && length$6 ? " " : # ~
    t1_chr($1,$prev_word)@gesx;
  # Dat: we remove \abreak here -- it will be kept in the presentation
  # `\ ' ~
  ## print "($S)\n";
  $S
}
#die t1_pres2alpha 'ABC\relax CS';
die unless t1_pres2alpha('\IeC{\H o}fels\IeC {\\\'e}ge') eq "\xAEfels\xE9ge";
die unless '42\tt \foo ~{%}�#^prev�bar{ }{.}          prev\lowtilde +�+prev'eq
  t1_pres2alpha # Unit test
  ('{\upshape 42}\verb+\tt+\foo\textasciitilde \{\%\}\ss\#\^{}prev{\"{o}}'.
  '\emph{bar}{ }{.}~\ \!\,\thinspace\negthinspace\enspace\enskip\quad\qquad'.
  '\lowtilde{}\lowtilde+\~o+\~{}','prev');


# ASCII unchanged
# textasciicircum 94 textasciitilde 126 textbackslash 92 textbar 124
# textbraceleft 123 textbraceright 125
# textdollar 36
# textgreater 62 textless 60
# textquoteleft 96 textquoteright 39
# textunderscore 95 textquotedbl 34

# vvv Dat: \'i doesn't work in OT1 encoding; \'\i is needed
# vvv Dat: we emit '\DJ ' rather than '\DJ{}' so ligatures and kerning pairs won't be broken
my @t1_inascii_list=split',',q{+,+,+,+,+,+,+,+,+,+,+,+,+,\quotesinglbase ,}.
q{\guilsinglleft ,\guilsinglright ,\textquotedblleft ,\textquotedblright ,}.
q{\quotedblbase ,\guillemotleft ,\guillemotright ,{--},{---},\textcompwordmark ,\char24 ,}.
q{\i ,\j ,ff,fi,fl,ffi,ffl,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,}.
q{+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,}.
q{+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,+,\-,\u A,\k A,\'C,}.
q{\v C,\v D,\v E,\k E,\u G,\'L,\v L,\L ,\'N,\v N,\NG ,\H O,\'R,\v R,\'S,\v S,}.
q{\c S,\v T,\c T,\H U,\r U,\"Y,\'Z,\v Z,\.Z,IJ,\.I,\dj ,\textsection ,\u a,}.
q{\k a,\'c,\v c,\v d,\v e,\k e,\u g,\'l,\v l,\l ,\'n,\v n,\ng ,\H o,\'r,\v r,}.
q{\'s,\v s,\c s,\v t,\c t,\H u,\r u,\"y,\'z,\v z,\.z,ij,\textexclamdown ,}.
q{\textquestiondown ,\textsterling ,\`A,\'A,\^A,\~A,\"A,\r A,\AE,\c C,\`E,\'E,}.
q{\^E,\"E,\`I,\'I,\^I,\"I,\DJ ,\~N,\`O,\'O,\^O,\~O,\"O,\OE ,\O,\`U,\'U,\^U,}.
q{\"U,\'Y,\TH ,\SS ,\`a,\'a,\^a,\~a,\"a,\r a,\ae ,\c c,\`e,\'e,\^e,\"e,\`\i ,\'\i ,}.
q{\^\i ,\"\i ,\dh ,\~n,\`o,\'o,\^o,\~o,\"o,\oe ,\o,\`u,\'u,\^u,\"u,\'y,\th ,\ss };

#** @return $_[0] with "�" converted to "\\'a" etc, to be printed into the
#**         .ind file
sub t1_inascii($) {
  my $S=$_[0];
  # Dat: \177 is /sfthyphen (tex256.enc)
  $S=~s/([\015-\037\177-\377])/$t1_inascii_list[ord$1]/ge;
  $S
}

##** Example: 'a\" o' -> '�'
##** @return $_[0] with TeX T1 ligatures substituted back to normal letters
#sub t1_nolig($) {
#  my $S=$_[0];
#  my @L=qw{ff fi fl ffi ffl};
#  $S=~s@([\033-\037])@$L[ord($1)-033]@ge;
#  $S
#}
#die t1_chr 'IeC {\'\i }';
# die t1_pres2alpha q~az \IeC {\'\i }r\H os~;
# die t1_inascii t1_pres2alpha q~az \IeC {\'\i }r\H os�~;
#die t1_inascii "Kovacs Pal (1946--1994)";
# die t1_pres2alpha "Kovacs Pal (1846--1994)";
#die t1_nolig(t1_pres2alpha('hello\v t, world'."\035"));
# Unittest: die unless t1_pres2alpha(q~{\' {\i {}}r\'asm\'od~) eq "{�r�sm�d";
# Unittest: die unless t1_pres2alpha(q~{\'{\i}t\'{e}letkalkulus~) eq "{�t�letkalkulus";

# ---

my %hu_t1;
my %hu_t2;
my $hu_r1;
my $hu_r2;

#** Hungarian technical grouping, closer to makeindex(1); but
#** makeindex(1) says os < �n < �r (husort.pl cannot do this);
#** Hungarian normal grouping ($do_vowel_equiv=0, -a) says: �r < os < �n 
#** Hungarian technical grouping says ($do_vowel_equiv=1): �n < �r < os
my $do_vowel_equiv=1;

#** Initialize transliteration of Hungarian language
#** from Latin-[12] character set.
#**
#** To be used in --sort-hu only.
sub hu_latin_init() { # initialize %hu_t1, %hu_t2 and $hu_r2
  my $I;
  # <empty-string> 1
  # <whitespace> 2
  # <rest 0..32,127,128..255 > 3
  # <punctuation !"#$%&'()*+,-./  :;<=>?@ [\]^_` {|}~ > 4
  # <digit> y
  # <beginning-digit> 5
  # Dat: we establish (decimal) numerical ordering by specifying that
  #      punctuation<digit, empty-string<digit and digit==digit
  %hu_t2=();
  for ($I=0;$I<32;$I++) { $hu_t2{chr $I}='3' }
  for ($I=33;$I<127;$I++)  { $hu_t2{chr $I}='4' }
  for ($I=127;$I<255;$I++) { $hu_t2{chr $I}='3' }
  @hu_t2{'\0',' ','\011','\012','\013','\014','\015'}=(2,2,2,2,2,2,2);
  my @L=qw(
    0 y   1 y   2 y   3 y   4 y   5 y   6 y   7 y   8 y   9 y
    a A   A A   � B   � B   � C   � C
    b D   B D
    ccs FF   CCS FF   Ccs FF   cs F   CS F   Cs F   c E   C E
    ddzs II   DDZS II   Ddzs II   dzs I   DZS I   Dzs   I
    ddz HH   DDZ HH   Ddz HH   dz H   DZ H   Dz H   d G   D G
    e J   E J   � K   � K   f L   F L   ggy NN   GGY NN   Ggy NN
    gy N   GY   N   Gy   N   g M   G M   h O   H O
    i P   I P   � Q   � Q   j R   J R   k S   K S
    lly UU   LLY UU   Lly UU   ly U   LY U   Ly U   l T   L T   m V   M V
    nny XX   NNY XX   Nny XX   ny X   NY X   Ny X   n W   N W
    o Y   O Y   � Z   � Z   � a   � a   � b   � b
    p c   P c   q d   Q d   r e   R e
    ssz gg   SSZ gg   Ssz gg   sz g   SZ g   Sz g   s f   S f   � ff
    tty ii   TTY ii   Tty ii   ty i   TY i   Ty i   t h   T h
    u j   U j   � k   � k   � l   � l   � m   � m
    v n   V n   w o   W o   x p   X p   y q   Y q
    zzs ss   ZZS ss   Zzs ss   zs s   ZS s   Zs s   z r   Z r
  );
  $hu_r2="";
  for ($I=0; $I<@L; $I+=2) { # important to _increment_ $I
    $hu_t2{$L[$I]}=$L[$I+1];
    $hu_r2.=quotemeta($L[$I]).'|' if 1<length($L[$I]);
  }
  # ^^^ Dat: $hu_r2=join '|', map {quotemeta} grep {1<length} keys %hu_t2;
  # would be bad, because we need greedy regexp prefix order, i.e
  # (ccs|c) is good, and (c|ccs) is bad
  $hu_r2.="."; # /./s matches any character
  $hu_r1=$hu_r2;
  %hu_t1=%hu_t2;
  ##die $hu_r2;
  my @old=qw{� � � � � � � � � � � � � � � �}; # � � � �
  my @new=qw{a A a A e E i I o O � � u U � �};
  @hu_t1{@old}=@hu_t1{@new};
  if ($do_vowel_equiv) {
    @hu_t1{qw{� � � � � � � �}}=@hu_t1{qw{o O o O u U u U}};
  }
}

my $do_single_symbols=1;
my $do_detailed_punct=0; # like in earlier husort.pl, before Sun Jun 27 16:20:34 CEST 2004
#** Initialize transliteration of Hungarian language plus other chars
#** from TeX T1 character set. Sets %hu_t1, %hu_t2, $hu_r1, $hu_r2.
sub hu_t1_init() { # initialize %hu_t1, %hu_t2 and $hu_r2
  my $I;
  # <empty-string> 1 (not assigned; ``Jelek'')
  # <subitem-separator> 2 (not assigned; ``Elv�laszt�k'')
  # <whitespace> 3 (``Sz�k�z�k'')
  # <rest 0..32,127,128..255 > 4 (``Vez�rl�k'')
  # <punctuation !"#$%&'()*+,-./  :;<=>?@ [\]^_` {|}~ > 5 (``�r�sjelek'')
  # <digit> z
  # <beginning-digit> 6 (``Sz�mok'')
  # Dat: we establish (decimal) numerical ordering by specifying that
  #      punctuation<digit, empty-string<digit and digit==digit
  %hu_t2=();
  for ($I=0;$I<32;$I++) { $hu_t2{chr $I}='4' }
  #for ($I=33;$I<127;$I++)  { $hu_t2{chr $I}='5' }#!!
  if ($do_detailed_punct) { # !! beginning of index of lakk had strange order, only length of punctuation (|=:> =:) was important
    for ($I=33;$I<127;$I++)  { $hu_t2{chr $I}='5'.chr$I }
  } else {
    for ($I=33;$I<127;$I++)  { $hu_t2{chr $I}='5' }
  }
  for ($I=127;$I<255;$I++) { $hu_t2{chr $I}='4' }
  @hu_t2{"\0"," ","\011","\012","\013","\014","\015","\177"}=(3,3,3,3,3,3,3,3); # not in T1 encoding, but ASCII!
  @hu_t2{"\015","\016","\017","\020","\021","\022","\023","\024"}=(5,5,5,5,5,5,5,5); # in T1 encoding
  # Dat: e2=� (hu accent); e3=e: (foreign accent); c9=cs (hu multi); c3=c' (foreign accent);
  # Dat: vvv `z1 -> ya' is because numbers are coded as `z'
  my @L=split" ", "
    0 z   1 z   2 z   3 z   4 z   5 z   6 z   7 z   8 z   9 z
    \033 f1f1   \034 f1i1   \035 f1l1   \036 f1f1i1   \037 f1f1l1
    \215 n1g1   \306 a1e1   \320 d1j1   \327 o1e1   \336 t1h1   \346 a1e1
    \360 d1h1   \367 o1e1   \376 t1h1   \236 d1j1   \234 i1j1   \255 n1g1
    \274 i1j1   \337 s1s1   \377 s1s1
    \275 5 \276 5

    a a1   A a1
     \301 a2 \304 a3 \302 a4 \300 a5 \201 a6 \305 a7 \200 a8 \303 a9
     \341 a2 \344 a3 \342 a4 \340 a5 \241 a6 \345 a7 \240 a8 \343 a9
    b b1   B b1
    ccs c9c9   CCS c9c9   Ccs c9c9   cs c9   CS c9   Cs c9   c c1   C c1
     \202 c3 \307 c4 \203 c5   \242 c3 \347 c4 \243 c5
    ddzs d9d9   DDZS d9d9   Ddzs d9d9   dzs d9   DZS d9   Dzs   d9
     ddz d8d8   DDZ d8d8   Ddz d8d8   dz d8   DZ d8   Dz d8   d d1   D d1
     \204 d3 \244 d3
    e e1   E e1
     \311 e2 \313 e3 \312 e4 \310 e5 \206 e6 \205 e7
     \351 e2 \353 e3 \352 e4 \350 e5 \246 e6 \245 e7
    f f1   F f1
    ggy g9g9   GGY g9g9   Ggy g9g9   gy g9   GY g9   Gy g9   g g1   G g1
     \207 g3 \247 g3
    h h1   H h1
    i i1   I i1
     \315 i2 \317 i3 \316 i4 \314 i5 \235 i6
     \355 i2 \357 i3 \356 i4 \354 i5          \031 i7
    j j1   J j1   \032 j3
    k k1   K k1
    lly l9l9   LLY l9l9   Lly l9l9   ly l9   LY l9   Ly l9   l l1   L l1
     \210 l3 \211 l4 \212 l5   \250 l3 \251 l4 \252 l5
    m m1   M m1
    nny n9n9   NNY n9n9   Nny n9n9   ny n9   NY n9   Ny n9   n n1   N n1
     \213 n3 \214 n4 \321 n5   \253 n3 \254 n4 \361 n5
    o o1   O o1
     \323 o2 \326 o3 \216 o4 \325 o5 \324 o6 \322 o7 \330 o8
     \363 o2 \366 o3 \256 o4 \365 o5 \364 o6 \362 o7 \370 o8
    p p1   P p1
    q q1   Q q1
    r r1   R r1
     \217 r3 \220 r4   \257 r3 \260 r4
    ssz s9s9   SSZ s9s9   Ssz s9s9   sz s9   SZ s9   Sz s9   s s1   S s1
     \221 s3 \223 s4 \222 s5   \261 s3 \263 s4 \262 s5
    tty t9t9   TTY t9t9   Tty t9t9   ty t9   TY t9   Ty t9   t t1   T t1
     \225 t3   \224 t4   \265 t3 \264 t4
    u u1   U u1
     \332 u2 \334 u3 \226 u4 \333 u5 \331 u6 \227 u7
     \372 u2 \374 u3 \266 u4 \373 u5 \371 u6 \267 u7
    v v1   V v1
    w w1   W w1
    x x1   X x1
    y y1   Y y1
     \335 y3 \230 y4   \375 y3 \270 y4
    zzs yiyi   ZZS yiyi   Zzs yiyi   zs yi   ZS yi   Zs yi   z ya   Z ya
     \231 yc \233 yd \232 ye \271 yc \273 yd \272 ye
  ";
  # \231 /Zacute %	153
  # \271 /zacute %	185
  # \232 /Zcaron %	154
  # \272 /zcaron %	186
  # \233 /Zdotaccent %	155
  # \273 /zdotaccent %	187
  $hu_r2="";
  for ($I=0; $I<@L; $I+=2) { # important to _increment_ $I
    $hu_t2{$L[$I]}=$L[$I+1];
    $hu_r2.=quotemeta($L[$I]).'|' if 1<length($L[$I]);
  }
  # ^^^ Dat: $hu_r2=join '|', map {quotemeta} grep {1<length} keys %hu_t2;
  #   would be bad, because we need greedy regexp prefix order, i.e
  #   (ccs|c) is good, and (c|ccs) is bad
  # vvv This is not needed anymore for ``important space''
  #   $hu_t2{'\PtsGobble{}'}="";
  #   $hu_r2.=quotemeta('\PtsGobble{}')."|";
  ##die $hu_r2;
  $hu_r2.='\\\@empty |'; # (\@empty)
  $hu_t2{'\@empty '}=''; # Dat: C\@empty CS is C C S, not CS CS
  $hu_r2.='\\\\empty |';   #
  $hu_t2{'\empty '}=''; # Dat: C\empty CS is C C S, not CS CS
  
  %hu_t1=%hu_t2; @L=();

  if (!$do_single_symbols) { # !! separate option to neglect this...
    # vvv the characters ,"&'#()-./:;?[]{}_~ and [\020\021\022\023\024] are removed
    #     from level1, but not from level2.
    # vvv Dat: "! 2" was formerly specified -- is this because of subitems?
    push @L, split' ', "
      , #   \" #   & #   ' #   ( #   ) #   - #   . #   / #   : #   ; #   ? #
      [ #   ] #   _ #   ~ #    { #   } #   ! #
      \020 #   \021 #   \022 #   \023 #   \024 #";
  }
    
  # vvv !! ensure correct eliminate \textrm etc. from here
  #push @L, split' ', "
  #  \\{ #   \\} #   \$ #
  #  \\rm# #   \\it# #   \\sf# #   \\sc# #   \\sl# #   \\bf# #   \\tt# #
  #  \\textrm#{ #   \\textit#{ #   \\textsf#{ #   \\textsc#{ #   \\textsl#{ #
  #  \\textbf#{ #   \\texttt#{ #";

  # vvv Dat: `\$ #' is for `$n$-dimenzi�s'
  # vvv Imp: make single characters (here and up) except for o1, u1, y1 and ya this single character
  if ($do_vowel_equiv) {
    push @L,split' ', "
      \301 a1 \304 a1 \302 a1 \300 a1 \201 a1 \305 a1 \200 a1 \303 a1
      \341 a1 \344 a1 \342 a1 \340 a1 \241 a1 \345 a1 \240 a1 \343 a1
      \202 c1 \307 c1 \203 c1 \242 c1 \347 c1 \243 c1 \204 d1 \244 d1
      \311 e1 \313 e1 \312 e1 \310 e1 \206 e1 \205 e1 \351 e1 \353 e1
      \352 e1 \350 e1 \246 e1 \245 e1 \207 g1 \247 g1 \315 i1 \317 i1
      \316 i1 \314 i1 \235 i1 \355 i1 \357 i1 \356 i1 \354 i1 \031 i1
      \210 l1 \211 l1 \212 l1 \250 l1 \251 l1 \252 l1 \213 n1 \214 n1
      \321 n1 \253 n1 \254 n1 \361 n1 \323 o1 \325 o1 \324 o1 \322 o1
      \330 o1 \363 o1 \365 o1 \364 o1 \362 o1 \370 o1 \217 r1 \220 r1
      \257 r1 \260 r1 \221 s1 \223 s1 \222 s1 \261 s1 \263 s1 \262 s1
      \225 t1 \224 t1 \265 t1 \264 t1 \332 u1 \333 u1 \331 u1 \227 u1
      \372 u1 \373 u1 \371 u1 \267 u1 \335 y1 \230 y1 \375 y1 \270 y1
      \231 ya \233 ya \232 ya \271 ya \273 ya \272 ya ".
    # vvv Dat: this is buggy, because it maps `�' -> `o' and it maps foreign accented `o' to `o'.
    #"\326 o1 \216 o1 \366 o1 \256 o1 \334 u1 \226 u1 \374 u1 \266 u1";
    # vvv Dat: correct mapping: `�' -> `o', `�' -> `�', foreign accented `o' -> `o'
     "\326 o1 \216 o1 \366 o1 \256 o1 \334 u1 \226 u1 \374 u1 \266 u1"
  } else { # vowel_equiv=0; first-comparison of normal Hungarian ordering
    push @L,split' ', "
      \301 a1 \304 a1 \302 a1 \300 a1 \201 a1 \305 a1 \200 a1 \303 a1
      \341 a1 \344 a1 \342 a1 \340 a1 \241 a1 \345 a1 \240 a1 \343 a1
      \202 c1 \307 c1 \203 c1 \242 c1 \347 c1 \243 c1 \204 d1 \244 d1
      \311 e1 \313 e1 \312 e1 \310 e1 \206 e1 \205 e1 \351 e1 \353 e1
      \352 e1 \350 e1 \246 e1 \245 e1 \207 g1 \247 g1 \315 i1 \317 i1
      \316 i1 \314 i1 \235 i1 \355 i1 \357 i1 \356 i1 \354 i1 \031 i1
      \210 l1 \211 l1 \212 l1 \250 l1 \251 l1 \252 l1 \213 n1 \214 n1
      \321 n1 \253 n1 \254 n1 \361 n1 \323 o1 \325 o1 \324 o1 \322 o1
      \330 o1 \363 o1 \365 o1 \364 o1 \362 o1 \370 o1 \217 r1 \220 r1
      \257 r1 \260 r1 \221 s1 \223 s1 \222 s1 \261 s1 \263 s1 \262 s1
      \225 t1 \224 t1 \265 t1 \264 t1 \332 u1 \333 u1 \331 u1 \227 u1
      \372 u1 \373 u1 \371 u1 \267 u1 \335 y1 \230 y1 \375 y1 \270 y1
      \231 ya \233 ya \232 ya \271 ya \273 ya \272 ya ".
     "\326 o3 \216 o3 \366 o3 \256 o3 \334 u3 \226 u3 \374 u3 \266 u3"
  }
  $hu_r1=$hu_r2;
  for ($I=0; $I<@L; $I+=2) {
    $L[$I]=~y/#/ /;
    $L[$I+1]=~y/#//d;
    ## print "$L[$I]..\n";
    $hu_r1.=quotemeta($L[$I]).'|' if 1<length($L[$I]) and !exists $hu_t1{$L[$I]};
    $hu_t1{$L[$I]}=$L[$I+1];
  }
  @hu_t1{"\0"," ","\011","\012","\013","\014","\015","\177"}=("")x8;

  # vvv Imp: move part of these defs to @L
  $hu_t1{'{ }'}="5"; # Dat: this is the ``important space''
   $hu_t2{'{ }'}="535";
   $hu_r1.=quotemeta('{ }')."|";
   $hu_r2.=quotemeta('{ }')."|";
  $hu_t1{'{.}'}="5"; # Dat: this is the ``important dot'' for numerals: `2{.}7'
   $hu_t2{'{.}'}="535";
   $hu_r1.=quotemeta('{.}')."|";
   $hu_r2.=quotemeta('{.}')."|";

  $hu_r1.="."; # /./s matches any character
  $hu_r2.="."; # /./s matches any character
}

my $do_ugly_ldc_nobreak=0;
sub hu_trans_nobin($) {
  my $R=$_[0];
  # !! vvv invent a gerenic solution for this
  # vvv Dat: ugly hack to avoid putting `csh' etc. into the `Cs' group
  if ($do_ugly_ldc_nobreak and
    $R=~m@^(?:ccs|cs|ddz|dz|ggy|gy|lly|ly|nny|ny|ssz|sz|tty|ty|zzs|zs)@i and # Dat: dzs starts with dz
    $R!~m@^(?:csillag|csomag|csuk�)@i) { # Dat: T1 encoding
    # csillagos parancs, csomag bet�lt�se, csomagok, csuk� atom
    substr($R,1,0)='\empty ' # break the double consonant
  }
  my $T=$R;
  # vvv Dat: y@A-Z@a-z@ is _required_
  $R=~s@($hu_r1)@$hu_t1{$1}@geos; $R=~s/^z/6/;
  $T=~s@($hu_r2)@$hu_t2{$1}@geos; $T=~s/^z/6/;
  $R.'1'.$T
}

#hu_t1_init();
#print hu_trans_nobin "\\", ";\n"; #: 515
#print hu_trans_nobin "-", ";\n"; #: 15
#print hu_trans_nobin 'ABCCS', "\n"; # A B CS CS
#print hu_trans_nobin 'ABC\empty CS', "\n"; # A B C CS
#print hu_trans_nobin 'AB\relax CCS', "\n";
#die

sub hu_trans_bin($) { ## <!-> hu_trans?
  # vvv encode English letters as themselves, and encode other characters as
  #     a digit + letter
  # vvv Dat: US-ASCII, ISO-8859-2 and TeX-T1 all have the property that
  #     a capital letter is ordered before its lowercase counterpart.
  #     Rule `S2b.' mentioned in the documentation strongly relies on this
  #     property.
  hu_trans_nobin($_[0]).'19z'.$_[0]
}

# ---

sub hu_test_less($$) { hu_trans_bin($_[0]) lt hu_trans_bin($_[1]) }
#** @return 0 iff the parameters are already in good order
sub hu_test_unsorted(@) {
  return 0 if @_<2;
  for (my $I=1;$I<@_;$I++) {
    print $_[$I].": ".hu_trans_bin($_[$I])."\n";
    return $I if! hu_test_less($_[$I-1], $_[$I]);
  }
  0
}

sub hu_test() {
  hu_t1_init();

  my @L=qw{
    0 2a  9b  28c  100d
    �dventi_k�d Ady A_fest�_hal�la
    �hitat  �hnlich  ahogy
    Alkalmi_vers
    b0 b2 b9 b2.7
    C Chianti Cholnoky Curie Czuczor
    Cs Csajkovszkij Cs�th_G�za Csehov Cs._Szab�_L�szl�
    ha hab�r habilit�ci� habitus
    hol
    holt
    horgony
    h�rukk
    Horv�t{_}Alad�r Horv�t{_}K�roly Horv�t{_}Zolt�n Horv�th{_}Imre Horv�th{_}P�ter
    Horw�t_Alad�r Horw�th_Imre Horw�th_P�ter Horw�t_K�roly Horw�t_Zolt�n
    hostess
    h�szirom
    hoszs
    hossz
    hoszsz
    hosszabb
    h�szt�r
    hova
    hov�
    hov�$2  hov�$9  hov�$28  hova$100
    hoz$2.  hoz$9.  hoz$28.  hoz$100.
    hozz$2a  hozz$9a  hozz$28a  hozz$100a
    h�
    h�r�g
    h�r�g
    h�r�g
    h�
    k�n
    K�na
    k�n�l
    Kov�cs kov�cs Kov�csn�
    Kov�cs_P�l
    Kov�cs_P�l{_}(1767--1812)
    Kov�cs_P�l{_}(1946--1994)
    Kov�cs_P�l_ford�t�
    Kov�cs_P�l_k�lt�
    Kov�cs_P�l_(1767--1812)
    Kov�cs_P�l_(1946--1994)
    K�hegyi
    m�zSzer�
    meztelen
    m�ZsZ�mla
    m�zsZer�
    m�zszer�
    munkaer� munkaer�-gazd�lkod�s munkaer�_n�lk�li munkaer�s
    nagz nagy
    Nagy_A._L�szl� Nagy_Albert Nagy_A._M�rton
    naggy� nagygyakorlat nagyj�b�l
    Nagy_S�ndor Nagy_Szab�_Istv�n Nagy_Zolt�n
    Sz�chenyi-hegy Sz�chenyi_Istv�n Sz�ch�nyi_K�nyvt�r Sz�chenyi_t�r
    ya y� yb yc ycs yd ydz ydzs ye y� yf yg ygy yh yi y� yj yk yl yly
    ym yn yny yo y� y� y� yp yq yr ys ysz yt yty yu y� y� y� yv yw yx yy yz yzs
  };
  for(@L){ y/����_/\216\256\226\266 / } # Dat: only for T1; map Latin2 hu -> T1

  my $UNS=hu_test_unsorted @L;
  die "error before $L[$UNS]\n" if $UNS;
  print "All OK\n";
}

# --- converting between arabic and roman

my %roman_value=qw{
  cm 900   dccc 800   dcc 700   dc 600   d 500   cd 400   ccc 300   cc 200   c 100
  xc 90    lxxx 80    lxx 70    lx 60    l 50    xl 40    xxx 30    xx 20    x 10 
  ix 9     viii 8     vii 7     vi 6     v 5     iv 4     iii 3     ii 2     i 1     iiii 4
}; $roman_value{""}=0;
#** @param $_[0] a string possibly containing a positive roman number
#**        (case insensitive)
#** @return the (arabic, decimal) value of the number, or 0 on parse error
sub roman2arabic($) {
  # Dat: roman digits: i 1 v 5 x 10 l 50 c 100 d 500 m 1000
  # Dat: `mlm' is invalid, `mcml' is 1950
  my $S=$_[0];
  # Dat: locale-dependent lc(), m//i
  return 0 if 0==length($S) or $S!~/\A(m*)(c[md]|d?|d?c{1,3})(x[cl]|l?|l?x{1,3})(i[xv]|v?|iiii|v?i{1,3})\Z(?!\n)/i;
  1000*length($1)+$roman_value{lc$2}+$roman_value{lc$3}+$roman_value{lc$4}
}

#** @param $_[0] a positive, decimal, arabic number
#** @return its lowercase roman equilavalent, � la TeX \romannumeral
sub arabic2roman($) {
  my $mod=$_[0]%1000;
  my $ret="m"x int($_[0]/1000);
  if    ($mod>=900) { $mod-=900; $ret.="cm" }
  if    ($mod>=500) { $mod-=500; $ret.="d" }
  if    ($mod>=400) { $mod-=400; $ret.="cd" }
  while ($mod>=100) { $mod-=100; $ret.="c" }
  if    ($mod>=90 ) { $mod-=90 ; $ret.="xc" }
  if    ($mod>=50 ) { $mod-=50 ; $ret.="l" }
  if    ($mod>=40 ) { $mod-=40 ; $ret.="xl" }
  while ($mod>=10 ) { $mod-=10 ; $ret.="x" }
  if    ($mod>=9  ) { $mod-=9  ; $ret.="ix" }
  if    ($mod>=5  ) { $mod-=5  ; $ret.="v" }
  if    ($mod>=4  ) { $mod-=4  ; $ret.="iv" }
  while ($mod>=1  ) { $mod-=1  ; $ret.="i" }
  $ret
}

# Unittest: die unless arabic2roman(roman2arabic("mMmcMXliIii")) eq "mmmcmxliv";
# Unittest: die unless roman2arabic("iv")==4;
# Unittest: die unless roman2arabic("vi")==6;

# --- main()

# Dat: avoid qw{} and the Perl warning: Possible attempt to separate words with commas
my %hu_t1_groups;
#my $do_single_symbols=1;
sub hu_groups_init() {
  %hu_t1_groups=split ' ',q!
    0 Ismeretlen
    1 Jelek
    2 Elv\'alaszt\'ok
    3 Sz\'ok\"oz\"ok
    4 Vez\'erl\H{o}k
    5 \'Ir\'asjelek
    6 Sz\'amok
    c9 Cs  d9 Dzs d8 Dz  g9 Gy  l9 Ly  n9 Ny  s9 Sz  t9 Ty
    yi Zs  ya Z  yb Z  yc Z  yd Z  ye Z  yf Z
    a A,~\'A   e E,~\'E  i I,~\'I
    o O,~\'O   o3 \"O,~\H{O}
    u U,~\'U   u3 \"U,~\H{U}
    b B   c C   d D   f F   g G   h H   j J   k K   l L   m M
    n N   p P   q Q   r R   s S   t T   v V   w W   x X   y Y
  !;
  # ^^^ Dat: u === \c u < �
  #  o1 O,~\'O   o2 O,~\'O   o3 \"O,~\H{O}   o4 \"O,~\H{O}   o O
  #  u1 U,~\'U   u2 U,~\'U   u3 \"U,~\H{U}   u4 \"U,~\H{U}   u U
  # Dat: \"Ures -> Jelek at Thu Mar 25 12:18:17 CET 2004
  if ($do_vowel_equiv) {
    $hu_t1_groups{a}='A';
    $hu_t1_groups{e}='E';
    $hu_t1_groups{i}='I';
    $hu_t1_groups{o}='O'; delete $hu_t1_groups{o1}; delete $hu_t1_groups{o2}; delete $hu_t1_groups{o3}; delete $hu_t1_groups{o4};
    $hu_t1_groups{u}='U'; delete $hu_t1_groups{u1}; delete $hu_t1_groups{u2}; delete $hu_t1_groups{u3}; delete $hu_t1_groups{u4}; # BUGFIX at Wed May 12 21:48:45 CEST 2004
  }
  if ($do_single_symbols) {
    $hu_t1_groups{1}=$hu_t1_groups{2}=$hu_t1_groups{4}=$hu_t1_groups{5}=
      'Jelek';
  }
}

#** @param $_[0] string returned by hu_trans_nobin() or al.
#** @return canonical letter group name (e.g "Numbers", "Specials", "S")
sub word_to_group($) {
  # return substr($_[0],0,1);
  ## print "$_[0]\n";
  my $s1=substr($_[0],0,1);
  my $s2=substr($_[0],0,2);
  my $ret=
    exists $hu_t1_groups{$s2} ? $hu_t1_groups{$s2} # two-char has priority
  : exists $hu_t1_groups{$s1} ? $hu_t1_groups{$s1}
  : undef; # formerly: uc(substr($_[0],0,1));
  if (!defined $ret) {
    print STDERR "warning: assuming letter group 0 for: $_[0] ($.)\n";
    $ret='0'
  }
  $ret
}

sub whoami() {
  "This is husort.pl (in makeindex mode), version $main::VERSION
Written and (C) <by pts\@fazekas.hu> from December 2002. GNU GPL. NO WARRANTY.\n"
}
sub usage() {
  die whoami."\nUsage: $0 --sort-hu [-a] <INPUT >OUTPUT
   or: $0 [-makeindex-opt...] TEX_JOB[.idx]  # creates TEX_JOB.ind
Useful options are:
-h       display this help message
-a       make a and \\'a (and other Hungarian accented vowels) different
-i       read stdin, write to stdout
-q       display only errors on the terminal
-r       don't aggregate page ranges (42--137)
-o <fn>  specify output file name
-s gind  group entries by their first letter
-C <opt> specify option or no-option for Using...\n"    
}

usage() if! @ARGV or $ARGV[0] eq '-h' or $ARGV[0] eq '--help';
if ($ARGV[0] eq'--sort-hu') {
  my $I = 1;
  if (@ARGV > $I and $ARGV[$I] eq '-a') {
    ++$I;
    $do_vowel_equiv=0;
  }
  die "$0: too many flags after --sort-hu\n" if $I != @ARGV;
  hu_latin_init();
  my @L=sort map {chomp; hu_trans_bin $_} <STDIN>;
  my @M;
  for (@L) {
    @M=split /19z/, $_, 2;
    print "$M[1]\n";
  }
  exit;
}

my @latin2_decode=split/\|/,q#\nobreakspace |\k A|\u{}|\L|\textcurrency |\v L|\'S|\S|\"{}|\v S|\c S|\v T|\'Z|\-|\v Z|\.Z|\textdegree |\k a|\k\ |\l|\'{}|\v l|\'s|\v{}|\c\ |\v s|\c s|\v t|\'z|\H{}|\v z|\.z|\'R|\'A|\^A|\u A|\"A|\'L|\'C|\c C|\v C|\'E|\k E|\"E|\v E|\'I|\^I|\v D|\DJ|\'N|\v N|\'O|\^O|\H O|\"O|$\times$|\v R|\r U|\'U|\H U|\"U|\'Y|\c T|\ss|\'r|\'a|\^a|\u a|\"a|\'l|\'c|\c c|\v c|\'e|\k e|\"e|\v e|\'\i|\^\i|\v d|\dj|\'n|\v n|\'o|\^o|\H o|\"o|$\div$|\v r|\r u|\'u|\H u|\"u|\'y|\c t|\.{}#;
#for (@latin2_decode) { t1_chr(substr($_,1),"") } die;

# --- Replacement makeindex(1)
#
# See the documentatation of makeindex(1) for details of commandline options.

#** input filename with .idx, or `-' for STDIN
my $FN_IDX;
#** output filename with .ind, or `-' for STDOUT
my $FN_IND;
#** @param $_[0] input filename
sub set_job($) {
  die "$0: multiple .tex jobs specified\n" if defined $FN_IDX;
  $FN_IDX=$_[0];
  $FN_IDX.='.idx' if $FN_IDX ne '-' and $FN_IDX!~m@[.][^\\/.]*\Z(?!\n)@;
  if (!defined $FN_IND) {
    $FN_IND=$_[0];
    $FN_IND=~s@[.][^\\/.]*\Z(?!\n)@@;
    $FN_IND.='.ind' if $FN_IDX ne '-';
  }
}

#** filename with .ind or `-' for STDOUT
my $do_quiet=0;
my $do_shadow_untagged=0;
my $do_group_headings=0;
my $do_aggregate_pages=1;
#** transform "^^I" to "\009", to be used without inputenc.sty
my $do_circum2=1;
my $do_latin2=1;
my $do_separate_tags=0;
my $do_page_warnings=1;
#** Page numbers are in matbook exerciseref syntax (<vol.ume>.<chapter>.<number>)
my $do_pages_are_exerciseref=0;

my $had_opt_s=0;
my $had_opt_o=0;
sub do_argv($$) {
  my $is_too_late=$_[0];
  my $argv=$_[1];
  my $I;
  $had_opt_s=0 if $is_too_late;
  for ($I=0;$I<@$argv;$I++) {
    if ($argv->[$I] eq '--') { $I++; last }
    elsif ($argv->[$I] eq '-' or substr($argv->[$I],0,1)ne'-') { set_job $argv->[$I] }
    elsif ($argv->[$I] eq '-i') { goto TOO_LATE if $is_too_late; set_job '-'; }
    elsif ($argv->[$I] eq '-q') {
      if ($is_too_late) { TOO_LATE: die "$0: too late to set $argv->[$I]\n" }
      $do_quiet=1
    } elsif ($argv->[$I] eq '-r') { $do_aggregate_pages=0; }
      elsif ($argv->[$I] eq '-a') { $do_vowel_equiv=0; }
    elsif (substr($argv->[$I],0,2) eq '-C') {
      my $arg=(length($argv->[$I])>2 ? substr($argv->[$I],2) : $argv->[++$I]);
      die "$0: need arg for -C\n" if !defined $arg;
      $arg=lc $arg; $arg=~y@a-z0-9@@cd;
      my $v=1; $v=0 if $arg=~s@^no@@;
         if ($arg eq 'circum2') { $do_circum2=$v }
      elsif ($arg eq 'latin2') { $do_latin2=$v }
      elsif ($arg eq 'aggregatepages') { $do_aggregate_pages=$v }
      elsif ($arg eq 'vowelequiv') { $do_vowel_equiv=$v }
      elsif ($arg eq 'separatetags') { $do_separate_tags=$v }
      elsif ($arg eq 'pagewarnings') { $do_page_warnings=$v }
      elsif ($arg eq 'pagesareexerciseref') { $do_pages_are_exerciseref=$v }
      elsif ($arg eq 'groupheadings') { $do_group_headings=$v }
      elsif ($arg eq 'singlesymbols') { $do_single_symbols=$v }
      elsif ($arg eq 'shadowuntagged') { $do_shadow_untagged=$v }
      elsif ($arg eq 'uglyldcnobreak') { $do_ugly_ldc_nobreak=$v }
      elsif ($arg eq 'detailedpunct') { $do_detailed_punct=$v }
      else { die "$0: unknown arg for -C: $arg\n" }
    } elsif (substr($argv->[$I],0,2) eq '-o') {
      my $arg=(length($argv->[$I])>2 ? substr($argv->[$I],2) : $argv->[++$I]);
      die "$0: need arg for -o\n" if !defined $arg;
      die "$0: give only a single `-o'\n" if $had_opt_o++;
      die "$0: too late to write to stdout\n" if $is_too_late;
      # ^^^ Dat: we've already printed debug messages there
      $FN_IND=$arg;
    }
    elsif (substr($argv->[$I],0,2) eq '-s') {
      # Only `-snone', `-sgglo', `-sbbglo', `-sbbind' and `-sgind'  are supported
      my $arg=(length($argv->[$I])>2 ? substr($argv->[$I],2) : $argv->[++$I]);
      die "$0: need arg for -s" if !defined $arg;
      if ($arg eq 'none' or $arg eq 'gglo' or $arg eq 'bbglo') { $do_group_headings=0 }
      elsif ($arg eq 'bbind' or $arg eq 'gind') { $do_group_headings=1 }
      else { die "$0: unknown arg for `-s', try `-s gind' to get group headings\n" }
      die "$0: give only a single `-s'\n" if $had_opt_s++;
    }
    elsif ($argv->[$I]=~/^-[lcg]/) { print STDERR "warning: ignoring makeindex option: $argv->[$I]\n" }
    elsif ($argv->[$I]=~/^-[tp]/) {
      my $arg=length($argv->[$I])>2 ? substr($argv->[$I],2) : $argv->[++$I];
      die "$0: need arg for $argv->[$I]" if !defined $arg;
      print STDERR "warning: ignoring makeindex option: $argv->[$I]\n"
    } else { die "$0: unknown option: $argv->[$I]\n" }
  }
  # vvv Dat: will emit an other error message
  # die "$0: too late to set job\n" if $is_too_late and $I<@$argv;
  while ($I<@$argv) { set_job $argv->[$I++] }
}
  
do_argv 0, \@ARGV;
die "$0: missing .tex job arg\n" if! defined $FN_IDX;
die if! defined $FN_IND; # implied by $FN_IDX
if ($FN_IND eq'-') {
  die unless open FKI, ">&STDOUT";
  die unless open STDOUT, ">&STDERR";
  die unless open INFO, ">&STDERR";
} else {
  die unless open INFO, ">&STDOUT";
}
my $indfin=$FN_IND eq '-' ? 'stdout' : "file $FN_IND";

sub bye() {
  print INFO "Output written in $indfin.\nNo transcript written.\n" if !$do_quiet;
  exit 0;
}

sub show_using() {
  print "Using group_headings=$do_group_headings, aggregate_pages=$do_aggregate_pages, vowel_equiv=$do_vowel_equiv, circum2=$do_circum2, latin2=$do_latin2,\n".
        "      separate_tags=$do_separate_tags, single_symbols=$do_single_symbols, shadow_untagged=$do_shadow_untagged, ugly_ldc_nobreak=$do_ugly_ldc_nobreak,\n".
	"      detailed_punct=$do_detailed_punct, page_warnings=$do_page_warnings, pages_are_exerciseref=$do_pages_are_exerciseref.\n" unless $do_quiet;
}

print whoami."Not scanning style files.\n" unless $do_quiet;
show_using();
if ($FN_IDX eq '-') {
  print "Scanning stdin...\n" unless $do_quiet;
  die unless open F, "<&STDIN";
} else {
  print "Scanning input file $FN_IDX...\n" unless $do_quiet;
  die "$0: open $FN_IDX: $!\n" if! open F, "< $FN_IDX";
}

my $had_inits=0;
#** List of pages. t is the result of hu_trans_nobin().
#** Arrayref elements:
#**   0 page_number_processed (+1e9)
#**   1 page_number_unprocessed
#**   2 tag
#**   3 ent: hu_order,transd(alpha) . "\0" . pres "\0\0" . ...
#**   4 presentation string . "\0" . ...
#**   5 hu_trans_nobin(page_number)
#**   6 .idx-linenumber
#** Example:
#** @pages=@{[
#**   [1e9+5, 5, '', 'word', t, htnb, lineno]  # arabic (comes later than romans)
#**   [5, 'v', '', 'word', t, htnb, l]         # roman
#**   [1e9+5, 5, 'textbf', 'word', t, htnb, l] # tagged arabic
#**   [5, 'v', 'textit', 'word', t, htnb, l]   # tagged roman
#** ]};
my @pages;
#** $open_c{"word|)..."} is the open count of the first pending \index{word|(...}
my %open_c;
#** $open_at{"word|)..."} is the @pages entry of the first pending \index{word|(...}
my %open_at;
#** $open_last{$ent}=$tag, for \index{foo|)} of \index{foo|(textbf}
my %open_last;
my $lin_accepted=0;
my $lin_rejected=0;
my $ent;
sub B(){1000000000}
sub tr_keep($){$_[0]}
sub tr_uc($){uc $_[0]}
sub tr_ucfirst($){ucfirst $_[0]}
LINE:
while (defined($ent=<F>)) {
  if ($ent=~m@^\s*\\relax\s*$@ or $ent!~/\S/) { $lin_accepted++; next }
  if ($ent!~s@^\s*\\indexentry\s*[{]@!@
   or $ent!~s@[}]\s*[{]([^}]+)[}]\s*\Z(?!\n)@@) { # Dat: removes \n
    $lin_rejected++;
    print STDERR "warning: syntax error, ignoring line ($.)\n"; next
  }
  if ($ent=~y@\000-\037@@) {
    print STDERR "warning: control character in line ($.)\n";
  }
  if ($do_circum2) {
    # vvv Dat: accented character in \write, without \usepackage[...]{inputenc}
    $ent=~s@\^\^([0-9a-f][0-9a-f]|[A-`?])@length($1)==2 ? chr hex $1 : "\100"^$1 @ge;
    # Dat: TeX allows ^^5e^41 instead of ^^41 (but not ^^5e^^5e41), but we don't,
    #      since we're not a clone of the TeX input parser, but we're trying to
    #      parse TeX's output.
  }
  #print STDERR "($ent)\n";
  if ($do_latin2) {
    $ent=~s~([\xA0-\xFF])~t1_chr(substr($latin2_decode[ord($1)-0xA0],1),"")~ge;
    # ^^^ Dat: we need t1_chr so `"' won't be inserted
  }
  if (substr($ent,0,6)eq'!|run ') {
    if ($lin_accepted!=0) {
      $lin_rejected++;
      print STDERR "warning: `|run ' entry too late, ignoring line ($.)\n"; next
    }
    my($dummy,$cmd,@args)=split' ',$ent;
    if (!defined $cmd) {
      $lin_rejected++;
      print STDERR "warning: missing command for `|run ', ignoring line ($.)\n"; next
    }
    if ("/$cmd"=~m@/husort[.]pl\Z(?!\n)@) {
      do_argv 1, \@args;
      show_using();
      $lin_accepted++;
      next;
    } else {
      $lin_accepted++;
      die "$0: open/w $FN_IND: $!\n" if $FN_IND ne'-' and !open STDOUT, "> $FN_IND";
      # vvv emulate doc.sty for `makeindex -s gind'
      print '{\makeatletter\newif\ifscan@allowed}'."\n".
            '\providecommand\pfill{, }\providecommand\efill{\hfill\nopagebreak}'."\n";
      # vvv Dat: makeindex(1) ignores `-q' with `-i'
      unshift @args, '-q' if $do_quiet;
      unshift @args, '-i';
      die "$0: popen $cmd: $!\n" if !open PIPE, "| $cmd ".join'  ',@args;#'
      my $err=0;
      while (<F>) {
        $lin_accepted++; # Dat: can't do better
        if (!print PIPE) { $err=1; last }
      }
      die "$0: write to $cmd: $!\n" if !close(PIPE) or $err;
      print STDERR "Running $cmd OK\n" unless $do_quiet;
      goto BYE;
    }
  }
  my $pagenr=$1;
  $ent='!"'.substr($ent,-1) if length($ent)==2; # Dat: hack \indexentry{|} to \indexentry{"|}
  # Dat: now $ent is like 'a@b!"!@d\"!e@f|(textbf'
  $ent=~s/(\\"|"(.)|[\@!|])/substr($1,0,1)eq"\\"?$1 :
    substr($1,0,1)eq'"'?$2 : "\n$1"/seg;
  # Dat: now each special char (@ ! |) begins in its own line, and quoting "s
  #      have been removed, and
  #      $ent is like: a \n@ b \n! ! \n@ d\" \n! e \n@ f \n| ( t e x t b f
  my $tag=""; # Dat: default may be "", since \index{foo|} is meaningless anyway, makeindex(1) doesn't emit \{42} as page number (but 42)
  my $opentag=""; # '', '(' or ')'
  my $RI;
  if (0<=($RI=rindex($ent,"\n|"))) {
    $tag=substr($ent,$RI+2); substr($ent,$RI)="";
    $opentag=$1 if $tag=~s@\A([()])@@;
    # Dat: makeindex(1) rejects \indexentry{a|b@c}, husort.pl gives a warning
    if ($ent=~s@\n\|@|@g) {
      print STDERR "warning: multiple unquoted \"|\", using last ($.)\n";
    }
    if ($tag=~s~\n\@~\@~g) {
      print STDERR "warning: unquoted \"\@\" in tag ($.)\n";
    }
    if ($tag=~s~\n\!~\!~g) {
      print STDERR "warning: unquoted \"\!\" in tag ($.)\n";
    }
  }

  my $rapage;
  if ($do_pages_are_exerciseref and $pagenr=~/\A[A-Z][A-Za-z0-9]*[.][A-Z0-9]+[.]\d+[.]\d+\Z(?!\n)/) {
    # Imp: real, comparable numbering?
    $rapage=B+B;
  } elsif (0!=($rapage=roman2arabic($pagenr))) {
  } elsif ($pagenr!~/\A(?:0|[1-9][0-9]*)\Z(?!\n)/) { # Dat: arabic
    #print STDERR "warning: unknown page number, assuming 0: $pagenr\n";
    #$rapage=B;
    print STDERR "warning: unknown page number, assuming inf: $pagenr\n" if $do_page_warnings;
    $rapage=B+B;
  } else { $rapage=$pagenr+B }

  # Dat: now only \n@ and \n! are special, so we're ready to split subwords
  # !! what to do with \indexentry{?`@...} ?? why asked ??
  die unless substr($ent,0,2)eq"\n!";
  if (length($ent)==2) {
    $lin_rejected++;
    print STDERR "warning: empty entry, ignoring ($.)\n"; next LINE;
    # ^^^ shouldn't happen, we've already checked that
  }
  my $presd="";
  { my $ent2=$ent;
    my $alpha='~';
    my $pres;
    $ent="";
    while ($ent2=~/\n!(.*)(\n\@(.*))?/g) {
      if (defined $3 && length $3) {
        $alpha=$1; $pres=$3;
      } else {
        ##print STDERR "old: $1\n";
        $pres=$1; $alpha=t1_pres2alpha($pres,$alpha); # Dat: use prev value
        #print STDERR "$pres --> $alpha\n";
      }
      if ($pres=~y@\0@@d) {
        # Dat: \000 is invalid as \TeX input, anyway (T1 /grave accent)
        print STDERR "warning: removed \\0 from presentation ($.)\n";
      }
      if ($alpha=~y@\0@@d) {
        print STDERR "warning: removed \\0 from alphabetic ($.)\n";
      }
      if (0==length $pres) {
        print STDERR "warning: empty ".(0==length $alpha ? "subword" : "presentation")." ($.)\n";
      } elsif (0==length $alpha) {
        print STDERR "warning: empty alphabetic ($.)\n";
      }
      if (0==length$alpha) {
	$lin_rejected++;
	print STDERR "warning: empty alphabetic subword, ignoring line ($.)\n"; next LINE;
      }
      if (0==length$alpha) {
	$lin_rejected++;
	print STDERR "warning: empty presentation subword, ignoring line ($.)\n"; next LINE;
      }
      # Dat: this sorts `hello' before `hello, world!'
      if (!$had_inits) { # to this late to respect `|run '
	hu_t1_init();
	hu_groups_init();
	$had_inits=1;
      }
      #print STDERR "DEBUG: $alpha -> ".hu_trans_nobin($alpha)."\n";
      $ent.=hu_trans_nobin($alpha)."\0"; # Imp: only if doing Hungarian (i18n)
      ##print STDERR "ent==($ent)\n";
      $ent.="$alpha\0$pres\0\0";
      $presd.="$pres\0";
    }
  }

  if ($opentag eq ')') {
    $opentag="$ent\n\n$tag";
    if ($tag eq '' and !exists $open_at{$opentag} and exists $open_last{$ent}) {
      # adds makeindex(1) compatibility for short interval closes
      $tag=$open_last{$ent}[0]; $opentag="$ent\n$tag";
      delete $open_last{$ent} if! defined ($open_last{$ent}=$open_last{$ent}[1]);
    }
    if (!exists $open_at{$opentag}) {
      print STDERR "warning: rejecting unmatched closer \"|)\" ($.)\n";
      $lin_rejected++; next
    } elsif (0==--$open_c{$opentag}) { # add this entry unconditionally
      my $openpage=$open_at{$opentag};
      delete $open_at{$opentag};
      delete $open_c{$opentag};
      if ($openpage->[0]<B and ($rapage>B and $rapage<B+B) or ($openpage->[0]>B and $openpage->[0]<B+B) and $rapage<B) {
        print STDERR "warning: interval between roman and arabic, keeping endpoints: $openpage->[1] .. $pagenr ($.)\n";
      } elsif ($openpage->[0]>$rapage) {
        print STDERR "warning: decreasing interval, keeping endpoints: $openpage->[1] .. $pagenr ($.)\n";
      } elsif ($openpage->[0]==B+B or $rapage==B+B) {
        print STDERR "warning: interval with unknown endpoint, keeping them: $openpage->[1] .. $pagenr ($.)\n";
      } elsif ($rapage<B) { # roman interval
        my $A=$openpage->[0];
        my $extra=(uc($pagenr) eq $pagenr) ? \&tr_uc : (ucfirst($pagenr) eq $pagenr) ? \&tr_ucfirst : \&tr_keep;
        # ^^^ Dat: detecting ucfirst() is unstable if length()==1
        while (++$A<$rapage) { push @pages, [$A, $extra->(arabic2roman($A)), $tag, $ent, $presd, hu_trans_nobin($A), $.] }
      } else { # arabic interval
        my $A=$openpage->[0];
        while (++$A<$rapage) { push @pages, [$A, $A- B(), $tag, $ent, $presd, hu_trans_nobin($A), $.] }
      }
    }
  }
  $lin_accepted++;
  push @pages, [$rapage, $pagenr, $tag, $ent, $presd, hu_trans_nobin($pagenr), $.];
  if ($opentag eq '(') {
    $opentag="$ent\n$tag";
    $open_at{$opentag}=$pages[-1] if! defined $open_c{$opentag};
    # $open_at{$opentag}[0]=$rapage if $rapage<$open_at{$opentag}[0]; # would do harm, don't modify
    $open_c{$opentag}++; # implements undef++
    $open_last{$ent}=[$tag,$open_last{$ent}]; # form a linked list
  }
}
die unless close F;
while (my($k,$v)=each %open_at) {
  print STDERR "warning: keeping only first for unmatched \"|(\" for word: $v->[3]\n";
  # Dat: prints sorted variant
}
%open_at=();

unless ($do_quiet) {
  print "  ($lin_accepted lines accepted, $lin_rejected rejected)\n";
  print "Sorting entries...\n";
}
## for (@pages) { print "@{$_}.\n" } die 42;
# Dat: the TeXbook orders first by page number, then by tag
# vvv makeindex(1) emits `\textbf{4, 5}, \see{you}{5}, \textbf{6}' instead
#     of `\textbf{4--6}, \see{you}{5}'. So do we.
@pages=sort { # ORDER BY
  $a->[3] cmp $b->[3] or # hu_order: alphabetic then presentation
  $a->[0] <=> $b->[0] or # page_number_processed
  $a->[5] cmp $b->[5] or # hu_trans_nobin(page_number_processed)
  $a->[1] cmp $b->[1] or # page_number_unprocessed
  $a->[2] cmp $b->[2] or # tag
  $a->[6] <=> $b->[6]    # .idx-linunumber (emitting order in document)
} @pages;
# ^^^ the difference between `cmp and `<=>' is important
# ^^^ $ent must be taken into account so Bab < bab
# ^^^ $ent is better than hu_trans_bin(), because \index{foo} < \index{foo@a}

print "Generating output $indfin...\n" unless $do_quiet;
die "$0: open/w $FN_IND: $!\n" if $FN_IND ne'-' and !open FKI, "> $FN_IND";
# vvv Allow english style, w/o \enskip and \IdxGroupHead etc.
die if !print FKI <<'EDDIG';
\begin{theindex}

\makeatletter
% vvv Separates word from list of pages
\expandafter\ifx\csname IdxPages\endcsname\relax \let\IdxPages\enskip \fi
% vvv Separates subwords after \subsubitem
\expandafter\ifx\csname IdxSubWord\endcsname\relax \def\IdxSubWord{, } \fi
\expandafter\ifx\csname lowtilde\endcsname\relax% only if undefined
  \def\lowtilde{{%
    \dimen0=1ex \font\f=cmex10 at 2.32261061433051\dimen0 \f%
    \lower 0.9ex\hbox{e}%
  }}%
\fi%
\let\OrigTildeAccent\~
\def\~#1{\ifx\hfuzz#1\hfuzz\lowtilde\else\OrigTildeAccent{#1}\fi}
EDDIG
die if $do_group_headings and !print FKI <<'EDDIG';
% vvv Starts a new letter group
\expandafter\ifx\csname IdxGroupHead\endcsname\relax
  \def\IdxGroupHead#1{\indexspace\par\noindent\hfil{\bfseries#1}\par\nopagebreak\smallskip}
\fi
EDDIG
## for (@pages) { print "@{$_}.\n" } die 42;
my $last_group="";
my $G;
my $ent_written=0;
#** Presentation items separated on `!'
my @pres;
my @ind_items=('\item ','\subitem ','\subsubitem ');
my($I,$J);
#** Used in the loop.
my $PGI;
my $last_ent="";
for ($PGI=0;$PGI<@pages;) {
  ## @M=split /19z/, $_, 2;
  # vvv Imp: correct this if not using Hungarian (i18n)
  $G=word_to_group($pages[$PGI][3]); # first alphabetic char, translated
  my $tag=$pages[$PGI][2];
  $ent=$pages[$PGI][3]; # alphabetic and presentation
  @pres=split/\0+/,$pages[$PGI][4]; # presentation list
  if ($G ne $last_group) {
    $last_group=$G;
    #print FKI "\n  \\noindent\\hfil{\\bfseries $last_group}\\par\\nopagebreak" if $do_group_headings;
    #print FKI "\n  \\indexspace\n\n";
    print FKI "\n  ".($do_group_headings ? "\\IdxGroupHead{$last_group}\n\n" : "\\indexspace\n\n");
  }
  ## print keys %{$pages{$ent}};  print ";; $ent\n";

  # Find number of upper headings to be printed
  die unless ($ent^$last_ent)=~/\A(\0*)/;
  $J=rindex($ent,"\0\0",length$1); # Dat: points to the beginning of "\0\0"
  $I=0; while ($J>0) { $I++; $J=rindex($ent,"\0\0",$J-2) }

  my $ent_ok_p=1;
  { # Sanity check whether output causes TeX syntax error
    my($J,$S,$T,$U,$I,$okp,@B);
    my %r=qw|( \( ) \) [ \[ ] \]|;
    for ($J=0;$J<@pres;$J++) {
      $S="" if !defined($S=$ind_items[$J]);
      $S="{$S} ".t1_inascii($pres[$J]); # '{\subitem}foo'
      $okp=1;
      # Dat: vvv even \t and \n are disallowed. Fine.
      if ($S=~y@\000-\037\177-\237@@d) { $okp=0; print STDERR "warning: control char removed from subword\n" }
      if ($S=~y@\240-\377@@) { $okp=0; print STDERR "warning: accented char in subword\n" }
      $T=$S;
      if ($T=~s@\\\Z(?!\n)@@) { $okp=0; print STDERR "warning: unterminated backslash in subword\n" }
      $T=~s~\\([^a-zA-Z\@]|[a-zA-Z\@]+ ?)~exists$r{$1}?$r{$1}:"X"~sge; # remove control sequences
      ## print STDERR "TT=($T)\n";
      if ($T=~y@\%@@) { $okp=0; print STDERR "warning: \% in subword\n" }
      $T=~s@[^{}\$\\\[\]\(\)]+@@g;
      @B=('');
      # Dat: ignore \[ in braces: allow \Q{\[}, but disallow `\[' 
      while ($T=~m@([{}\$])|\\([\[\]\(\)])@g) {
        if (defined $1 and $1 eq '{') { push @B, $1 }
	elsif (defined $1 and $1 eq '}') { last if '{'ne pop@B or !@B }
	elsif (defined $1 and $1 eq '$' and $B[-1] eq '$') { pop @B; last if !@B }
	elsif (defined $1 and $1 eq '$') { push @B, $1 }
	elsif (defined $2 and $2 eq '(') { next if $B[-1] eq '{'; last if $B[-1]eq'(' or $B[-1]eq'[' or $B[-1]eq'$'; push @B, $2 }
	elsif (defined $2 and $2 eq '[') { next if $B[-1] eq '{'; last if $B[-1]eq'(' or $B[-1]eq'[' or $B[-1]eq'$'; push @B, $2 }
	elsif (defined $2 and $2 eq ']') { next if $B[-1] eq '{'; last if '('ne pop@B or !@B }
	elsif (defined $2 and $2 eq ')') { next if $B[-1] eq '{'; last if '['ne pop@B or !@B }
      }
      if (@B!=1) {
        print STDERR "co=$T.\n";
        print STDERR "warning: ".(@B<1 ? "too many" : "not enough")." braces in subword\n"; $okp=0;
      }
      if (!$okp) {
        $ent_ok_p=0;
        print STDERR "warning:   offending subword: $S\n";
      } ## else { print "($S)\n" }
    }
  }
  my $J=$PGI+1;
  if (!$ent_ok_p) {
    $J++ while $J<@pages and $pages[$J][3] eq $ent;
    my $S=$J==$PGI+1 ? 'entry' : ($J-$PGI).' entries';
    print STDERR "warning: had problems, skipping $S ($pages[$PGI][6])\n";
    $PGI=$J; next
  }

  # vvv Print them (print `foo, bar' if missing for \index{foo!bar!baz})
  for (; $I<$#pres and $I<$#ind_items; $I++) { # Dat: sic `$I<$#'
    print FKI "  ".("  "x$I).$ind_items[$I].t1_inascii($pres[$I])."\n";
    # ^^^ no page number yet
  }
  ##print "processing word @pres $PGI=($pages[$PGI][6]).\n";
  splice @pres, 0, $I;
  print FKI "  ".("  "x$I).$ind_items[$I].t1_inascii(join '\IdxSubWord ', @pres);
  my @OL;
  while (1) {
    if ($do_shadow_untagged and 0==length$tag) {
      # A page number |textbf shadows a normal page number, but not a |textit
      $J++ while $J<@pages and $pages[$J][3] eq $ent and 0==length$pages[$J][2]
	and $pages[$J][1]eq$pages[$J-1][1];
      if ($J!=@pages and $pages[$J][3] eq $ent and 0!=length$pages[$J][2]
        and $pages[$J][1]eq$pages[$J-1][1]) { $tag=$pages[$J++][2] }
      $PGI=$J-1;
    }
    if ($do_aggregate_pages) {
      $J++ while $J<@pages and $pages[$J][3] eq $ent and $pages[$J][2] eq $tag and
        ($pages[$J][0]!=B+B ? $pages[$J][0]<=$pages[$J-1][0]+1 : $pages[$J][1]eq$pages[$J-1][1]);
	# Dat:                             ^^^ spot the difference
    } else {
      $J++ while $J<@pages and $pages[$J][3] eq $ent and $pages[$J][2] eq $tag and
        ($pages[$J][0]!=B+B ? $pages[$J][0]==$pages[$J-1][0]+1 : $pages[$J][1]eq$pages[$J-1][1]);
    }
    ##print "found pages $PGI...$J $pages[$PGI][0] .. $pages[$J-1][0]\n";
    my $pre=""; my $post=""; # {
    if ($tag ne '') { $pre="\\".t1_inascii($tag)."\{"; $post='}' } # Dat: t1_inascii() for \index{guard|see{�r}}
    # ^^^ Dat: makeindex(1) emits \see twice; so do we: `\item \angol {free software}, \see{szabad szoftver}{6}, \see{szabad szoftver}{12})
    if ($pages[$PGI][0]==$pages[$J-1][0]) {
      push @OL, $pre.$pages[$PGI][1].$post;
    } elsif ($do_separate_tags) {
      if ($pages[$PGI][0]==$pages[$J-1][0]-1) {
	push @OL, "$pre$pages[$PGI][1]$post", "$pre$pages[$J-1][1]$post";
      } else {
	push @OL, "$pre$pages[$PGI][1]$post--$pre$pages[$J-1][1]$post";
      }
    } elsif ($pages[$PGI][0]==$pages[$J-1][0]-1) {
      # This is bug-to-bug compatibility mode with makeindex(1): page numbers
      # 44, 45, 47 are emitted as \textit{44, 45}, \textit{47}.
      # hyperref.sty works around the problem.
      # vvv makeindex(1) emits `\textbf{5--7}' or `\textbf{5, 6}', but it emits
      #     `\textbf{5}, \textbf{6}' if !$do_aggregate_pages. So do we.
      push @OL, "$pre$pages[$PGI][1], $pages[$J-1][1]$post"; # Dat: even the comma
    } else {
      push @OL, "$pre$pages[$PGI][1]--$pages[$J-1][1]$post";
    }
    if (@OL>1 and $OL[-2] eq $OL[-1]) {
      # Dat: e.g when duplicate \indexentry {y|see{g}}{0}
      print STDERR "warning: husort bug: repeating page number: $OL[-1] (@pres) ($pages[$PGI][6])\n";
      pop @OL
    }
    $PGI=$J;
    last if $J==@pages or $pages[$J][3] ne $ent;
    $tag=$pages[$J++][2];
  }
  my $OLS=join ", ", @OL;
  if (grep { 0==length } @OL) {
    print STDERR "warning: empty page number in the output (word of $pages[$PGI][6])\n";
    $OLS=join ", ", grep { 0!=length } @OL; 
  }
  print FKI "\\IdxPages $OLS\n";
  $last_ent=$ent;  $ent_written++;
}
print FKI "\n\\end{theindex}\n";
# Imp: \indexspace\iffalse 1\indexspace A\fi (??)
# Imp: ferror()
die unless close FKI;
if ($do_quiet) {}
elsif ($ent_written==1) { print "  (1 entry written)\n" }
else { print "  ($ent_written entries written)\n" }
#BYE: # Perl v5.8 SUXX
bye();

__END__
