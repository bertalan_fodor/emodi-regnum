use File::Copy;

# perl ekezeteket_normalizal.pl <sok_sok_bemeno_file_neve
while (<STDIN>) {
    $old_name = $_;
    $old_name =~ s/\n//g;
    s/ \\u A\\k A/ Á/g;
    s/\\u A\\k A/á/g;
    s/\\u A\\l /ó/g;
    s/\\'L\\'S/ő/g;
    s/\\u A\\v S/é/g;
    s/\\u A\\'s/ö/g;
    s/\\u A\\'z/ü/g;
    s/\\u A\\c s/ú/g;
    s/\\u A\\H U/Ö/g;
    s/\\u A/í/g;
    print "$_";
}
