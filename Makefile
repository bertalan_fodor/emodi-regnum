all: alt

first:
	lualatex rmtort-2015.tex; lualatex rmtort-2015.tex; makeindex -o rmtort-2015.nnd rmtort-2015.ndx; 
	@echo ""
	@echo "Now fix rmtort-2015.nnd by hand, then call make second."

second:
	lualatex rmtort-2015.tex; lualatex rmtort-2015.tex

alt:
	lualatex rmtort-2015.tex; lualatex rmtort-2015.tex; perl -x husort.pl -o rmtort-2015.nnd rmtort-2015.ndx; perl ekezetet_normalizal_2.pl <rmtort-2015.nnd >rmtort-2015.nnd_2; mv rmtort-2015.nnd_2 rmtort-2015.nnd; lualatex rmtort-2015.tex

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *~ rmtort-2015.pdf *.ptb *.ndx *.idx *.toc *.ilg *.nnd
